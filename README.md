# LeasePlus - Arrendamento 🌱

![Alt ou título da imagem](https://www.myfarm.com.br/wp-content/uploads/2021/07/865731.jpeg)

## <a href="https://youtu.be/kYpZFjY91Vc"> LeasePlus - Pitch </a><h3>

Site desenvolvido com o objetivo de facilitar o processo de venda e arrendamento de chácaras, sítios e fazendas, tornando de maneira simples a busca destas terras, sendo possível visualiza-lá sem ao menos estar no local.

<h2>Tecnologias usadas:  </h2>

| HTML | CSS | PYTHON | JS | 
 --------- | --------- | --------- | --------- |

<h2>Integrantes da equipe:</h2>

~~~php
Guilherme Valério
~~~
~~~php
Henrique Garcia
~~~
~~~php
João Paulo Padilha
~~~
~~~php
Lucas Tamagawa
~~~
~~~php
Mariana Galvão
~~~
