from flask import Flask,render_template
app=Flask(__name__)

@app.route('/')
def webbot():
    return render_template("webbot.html")

app.run(debug=True)